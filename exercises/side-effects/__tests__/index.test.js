const fs = require('fs');
const path = require('path');

const { upVersion } = require('../src/index.js');

const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);
const readFile = (filepath) => fs.readFileSync(filepath, 'utf-8');

const cases = [
  ['major', '2.0.0'],
  ['minor', '1.4.0'],
  ['patch', '1.3.3'],
];

let initialState;

beforeAll(() => {
  initialState = readFile(getFixturePath('package.json'));
});

afterEach(() => {
  fs.writeFileSync(getFixturePath('package.json'), initialState);
});

describe('update version', () => {
  test.each(cases)('when %p version used returns %p',
    (versionType, expectedVersion) => {
      const filePath = getFixturePath('package.json');

      upVersion(filePath, versionType);

      const { version } = JSON.parse(readFile(filePath));
      expect(version).toBe(expectedVersion);
    });

  it('default version is patch', () => {
    const filePath = getFixturePath('package.json');

    upVersion(filePath);

    const { version } = JSON.parse(readFile(filePath));
    expect(version).toBe('1.3.3');
  });

  it('throw error when path not found', () => {
    const notFoundPath = getFixturePath('not_existent');

    expect(() => upVersion(notFoundPath)).toThrowError();
  });
});
