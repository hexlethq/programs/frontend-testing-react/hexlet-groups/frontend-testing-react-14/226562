const fs = require('fs');

const updateVersion = (version, type) => {
  const mapToVersion = {
    patch: ([major, minor, patch]) => [major, minor, patch + 1],
    minor: ([major, minor]) => [major, minor + 1, 0],
    major: ([major]) => [major + 1, 0, 0],
  };

  const formatedVersion = version.split('.').map(Number);
  return mapToVersion[type](formatedVersion).map(String).join('.');
};

const upVersion = (path, type = 'patch') => {
  const fileData = fs.readFileSync(path, 'utf8');
  const parsedData = JSON.parse(fileData);

  const updatedVersion = updateVersion(parsedData.version, type);

  const newFileData = { ...parsedData, version: updatedVersion };
  fs.writeFileSync(path, JSON.stringify(newFileData));
};

module.exports = { upVersion };
