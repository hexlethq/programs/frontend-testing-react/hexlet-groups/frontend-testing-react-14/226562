const assert = require('power-assert');
const { flattenDepth } = require('lodash');

const example = [1, [2, [3, [4]], 5]];

const cases = [
  [2, [1, 2, 3, [4], 5]],
  [0, example],
  [1, [1, 2, [3, [4]], 5]],
  [Infinity, [1, 2, 3, 4, 5]],
  [-1, example],
];

cases.forEach(([depth, result]) => {
  assert.deepEqual(flattenDepth(example, depth), result);
});

assert.deepEqual(flattenDepth(example), [1, 2, [3, [4]], 5]);
