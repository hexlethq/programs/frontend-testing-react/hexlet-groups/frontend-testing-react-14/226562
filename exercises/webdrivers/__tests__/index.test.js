const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  test('main page works', async () => {
    await page.goto(appUrl, { waitUntil: 'domcontentloaded' });
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Simple Blog');
  });

  test('articles page works', async () => {
    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
    const articles = await page.$$('#articles > tbody > tr');
    expect(articles.length).toBeGreaterThan(1);
  });

  test('can see creation form', async () => {
    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
    await page.click('a[href="/articles/new"]');
    await page.waitForSelector('form');

    const html = await page.evaluate(() => document.body.innerHTML);
    expect(html).toContain('form');
  });

  test('create article', async () => {
    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
    await page.click('a[href="/articles/new"]');
    await page.waitForSelector('form');

    const formData = { name: 'Puppeteer', content: 'New content' };
    await page.type('#name', formData.name);
    await page.type('#content', formData.content);
    await page.click('[type="submit"]');
    await page.waitForSelector('#articles');

    const table = await page.$('#articles');
    const html = await page.evaluate((e) => e.innerHTML, table);
    expect(html).toContain(formData.name);
  });

  test('edit article', async () => {
    await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
    const articleLinks = await page.$$('td > a');
    await articleLinks[0].click();
    await page.waitForSelector('form');

    const newName = 'New name for article';
    await page.type('#name', newName);
    await page.click('[type="submit"]', { waitUntil: 'domcontentloaded' });
    await page.waitForSelector('#articles');

    const table = await page.$('#articles');
    const html = await page.evaluate((e) => e.innerHTML, table);
    expect(html).toContain(newName);
  });

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
