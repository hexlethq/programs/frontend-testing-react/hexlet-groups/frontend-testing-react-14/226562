const faker = require('faker');

const port = 5000;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

test('main page works', async () => {
  await page.goto(appUrl, { waitUntil: 'domcontentloaded' });
  await expect(page).toMatch('Simple Blog');
});

test('articles page works', async () => {
  await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
  const articles = await page.$$('[data-testid^="article-edit-link-"]');
  expect(articles.length).toBeGreaterThan(1);
});

test('can see creation form', async () => {
  await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
  await page.click('[data-testid="article-create-link"]');
  await page.waitForSelector('[data-testid="article-create-button"]');
  await expect(page).toMatch('Create article');
});

test('create article', async () => {
  await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
  await page.click('[data-testid="article-create-link"]');
  await page.waitForSelector('[data-testid="article-create-button"]');

  const name = faker.name.title();
  const content = faker.lorem.paragraph();
  await expect(page).toFillForm('form', {
    'article[name]': name,
    'article[content]': content,
  });
  await expect(page).toSelect('[name="article[categoryId]"]', '1');
  await page.click('[data-testid="article-create-button"]', { waitUntil: 'domcontentloaded' });
  await page.waitForSelector('[data-testid="article-create-link"]');

  await expect(page).toMatch(name);
});

test('edit article', async () => {
  await page.goto(appArticlesUrl, { waitUntil: 'domcontentloaded' });
  const articles = await page.$$('[data-testid^="article-edit-link-"]');
  await articles[0].click();
  await page.waitForSelector('[data-testid="article-update-button"]');

  const name = faker.name.title();
  await expect(page).toFillForm('form', {
    'article[name]': name,
  });
  await page.click('[data-testid="article-update-button"]', { waitUntil: 'domcontentloaded' });
  await page.waitForSelector('[data-testid="article-create-link"]');

  await expect(page).toMatch(name);
});
