import React from 'react';

import PriorityDropdown from './PriorityDropdown';

export default {
  component: PriorityDropdown,
  title: 'Dropdown',
}

const Template = args => <PriorityDropdown {...args} />

export const Default = Template.bind({});
Default.args = {
  variant: 'warning',
  dropDirection: 'down',
}

export const Right = Template.bind({});
Right.args = {
  ...Default.args,
  dropDirection: 'right',
}

export const Small = Template.bind({});
Small.args = {
  ...Default.args,
  size: 'sm',
}

export const Large = Template.bind({});
Large.args = {
  ...Default.args,
  size: 'lg',
}
