const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

const integerOrFloat = fc.oneof(fc.float(), fc.integer());

test('sorted', () => {
  fc.assert(
    fc.property(fc.array(integerOrFloat), (data) => {
      const result = sort(data);

      expect(result).toBeSorted();
    }),
  );
});

test('for idempotency', () => {
  fc.assert(
    fc.property(fc.array(integerOrFloat), (data) => {
      expect(sort(data)).toEqual(sort(sort(data)));
    }),
  );
});

test('for length', () => {
  fc.assert(
    fc.property(fc.array(integerOrFloat), (data) => {
      expect(sort(data).length).toEqual(data.length);
    }),
  );
});
