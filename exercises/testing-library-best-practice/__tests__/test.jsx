// @ts-check

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import faker from 'faker';
import _ from 'lodash';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.enableNetConnect();
});

const addTask = () => {
  const taskName = faker.lorem.word();
  const textBox = screen.getByRole('textbox');
  const task = { id: _.uniqueId(), text: taskName, state: 'active' };

  nock(host)
    .post('/tasks')
    .reply(201, task);

  userEvent.type(textBox, taskName);
  userEvent.click(screen.getByRole('button', { name: /add/i }));

  return task;
};

const finishTask = ({ id, text }) => {
  nock(host)
    .patch(`/tasks/${id}/finish`)
    .reply(200, { id, text, state: 'finished' });

  const taskLink = screen.getByRole('link', { name: text });
  userEvent.click(taskLink);
};

const activateTask = ({ id, text }) => {
  nock(host)
    .patch(`/tasks/${id}/activate`)
    .reply(200, { id, text, state: 'active' });

  userEvent.click(screen.getByRole('link', { name: text }));
};

describe('initial state of todo app', () => {
  test('no tasks', () => {
    nock(host)
      .get('/tasks')
      .reply(200, []);
    render(<TodoBox />);

    expect(screen.getByRole('textbox')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /add/i })).toBeInTheDocument();
  });

  test('with tasks', async () => {
    const activeTask = faker.lorem.word();
    const finishedTask = faker.lorem.word();
    nock(host)
      .get('/tasks')
      .reply(200, [{ id: _.uniqueId(), text: activeTask, state: 'active' }, { id: _.uniqueId(), text: finishedTask, state: 'finished' }]);
    render(<TodoBox />);

    const activeTaskContainer = await screen.findByTestId('actived-tasks');
    const finishedTaskContainer = await screen.findByTestId('finished-tasks');

    expect(within(activeTaskContainer).getByRole('link', { name: activeTask })).toBeVisible();
    expect(within(finishedTaskContainer).getByRole('link', { name: finishedTask })).toBeVisible();
  });
});

describe('todo operations', () => {
  beforeEach(() => {
    nock(host)
      .get('/tasks')
      .reply(200, []);
    render(<TodoBox />);
  });

  test('adding tasks', async () => {
    const task1 = addTask();
    const task2 = addTask();
    await waitFor(() => expect(nock.isDone()).toBe(true));

    expect(screen.getByRole('link', { name: task1.text })).toBeVisible();
    expect(screen.getByRole('link', { name: task2.text })).toBeVisible();
  });

  test('finishing task', async () => {
    const task = addTask();
    await waitFor(() => expect(nock.isDone()).toBe(true));
    finishTask(task);

    const finishedTaskContainer = await screen.findByTestId('finished-tasks');
    expect(within(finishedTaskContainer).getByRole('link', { name: task.text })).toBeVisible();
  });

  test('reactivate finished task', async () => {
    const task = addTask();
    await waitFor(() => expect(nock.isDone()).toBe(true));
    finishTask(task);
    await waitFor(() => expect(nock.isDone()).toBe(true));
    activateTask(task);

    const activatedContainer = await screen.findByTestId('actived-tasks');
    expect(within(activatedContainer).getByRole('link', { name: task.text })).toBeVisible();
  });
});
