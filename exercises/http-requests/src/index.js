const axios = require('axios');

const get = async (url) => {
  try {
    const response = await axios.get(url);

    return response.data;
  } catch (err) {
    throw new Error(err);
  }
};

const post = async (url, data) => {
  try {
    const response = await axios.post(url, data);

    return response.data;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = { get, post };
