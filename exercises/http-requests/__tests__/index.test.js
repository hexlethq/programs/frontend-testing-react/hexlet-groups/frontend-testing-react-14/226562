const nock = require('nock');
const axios = require('axios');

const { user, users } = require('../__fixtures__/users');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');

const host = 'http://www.example.com';

let scope;
beforeAll(() => {
  scope = nock(host);
  nock.disableNetConnect();
});

afterAll(() => {
  nock.cleanAll();
  nock.enableNetConnect();
});

describe('CRUD operations', () => {
  it('get request', async () => {
    scope.get('/users')
      .reply(200, users);

    const actual = await get(`${host}/users`);

    expect(actual).toEqual(users);
    expect(nock.isDone()).toBe(true);
  });

  it('post request', async () => {
    scope.post('/users')
      .reply(201, user);

    const actual = await post(`${host}/users`, user);

    expect(actual).toEqual(user);
    expect(nock.isDone()).toBe(true);
  });

  it('throws error on 404', async () => {
    scope.get('/users')
      .reply(404);

    await expect(get(`${host}/users`)).rejects.toThrowError(Error);
    expect(nock.isDone()).toBe(true);
  });

  it('throws error on 500', async () => {
    scope.post('/users')
      .reply(500);

    await expect(post(`${host}/users`)).rejects.toThrowError(Error);
    expect(nock.isDone()).toBe(true);
  });
});
