const user = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
};

const users = [
  { firstname: 'Artem', lastname: 'Rakov', age: 23 },
  { firstname: 'Peter', lastname: 'Pan', age: 16 },
];

module.exports = { user, users };
