const cases = [
  [{ k: 'v2', a: 'a' }, { k: 'v', b: 'b' }, { k: 'v', a: 'a', b: 'b' }],
  [{}, { a: 1, b: 2 }, { a: 1, b: 2 }],
  [{ a: 1, b: 2 }, {}, { a: 1, b: 2 }],
];

test.each(cases)('target: %s, src: %s', (target, src, expected) => {
  const result = Object.assign(target, src);

  expect(result).toEqual(expected);
  expect(result).toBe(target);
});

test('multiple sources', () => {
  const target = { a: 1, b: 2 };
  const src1 = { b: 4, c: 5 };
  const src2 = { a: 6, d: 7 };

  const result = Object.assign(target, src1, src2);

  expect(result).toEqual({
    a: 6,
    b: 4,
    c: 5,
    d: 7,
  });
});

test('only shallow copies the objects', () => {
  const target = { a: 1, b: { c: 0 } };

  // eslint-disable-next-line prefer-object-spread
  const result = Object.assign({}, target);
  target.a = 2;
  target.b.c = 4;

  expect(result).toEqual({ a: 1, b: { c: 4 } });
});

test('raises error on read only property', () => {
  const src = Object.defineProperty({}, 'a', {
    value: 1,
    writable: false,
  });

  expect(() => Object.assign(src, { a: 2 })).toThrowError();
});
