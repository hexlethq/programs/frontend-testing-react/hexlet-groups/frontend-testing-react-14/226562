// @ts-check

require('@testing-library/jest-dom');
const faker = require('faker');
const { screen } = require('@testing-library/dom');
const userEvent = require('@testing-library/user-event').default;
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

let tasksContainer;
let listContainer;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  tasksContainer = document.querySelector('[data-container="tasks"]');
  listContainer = document.querySelector('[data-container="lists"]');
  run();
});

const addTask = (taskName) => {
  userEvent.type(screen.getByTestId('add-task-input'), taskName);
  userEvent.click(screen.getByTestId('add-task-button'));
};

const addList = (listName) => {
  userEvent.type(screen.getByTestId('add-list-input'), listName);
  userEvent.click(screen.getByTestId('add-list-button'));
};

test('main page', () => {
  expect(tasksContainer).toBeEmptyDOMElement();
  expect(listContainer).toHaveTextContent('General');
});

test('Can add task to default list', () => {
  const taskName = 'First task';

  addTask(taskName);

  expect(screen.getByTestId('add-task-input')).toHaveValue('');
  expect(tasksContainer).toHaveTextContent(taskName);
});

test('add multiple tasks', () => {
  const firstTask = faker.lorem.words();
  const secondTask = faker.lorem.words();

  addTask(firstTask);
  addTask(secondTask);

  expect(tasksContainer).toHaveTextContent(firstTask);
  expect(tasksContainer).toHaveTextContent(secondTask);
});

test('can create second list', () => {
  const firstTask = faker.lorem.words();
  const secondList = faker.lorem.words();

  addTask(firstTask);
  addList(secondList);
  userEvent.click(screen.getByTestId(`${secondList}-list-item`));

  expect(tasksContainer).toBeEmptyDOMElement();
});

test('can add task to second list', () => {
  const firstTask = faker.lorem.words();
  const secondList = faker.lorem.words();

  addList(secondList);
  userEvent.click(screen.getByTestId(`${secondList}-list-item`));
  addTask(firstTask);

  expect(tasksContainer).toHaveTextContent(firstTask);
});
