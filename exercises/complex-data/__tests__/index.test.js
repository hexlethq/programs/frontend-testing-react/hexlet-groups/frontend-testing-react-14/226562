const faker = require('faker');

it('have correct types', () => {
  const transaction = faker.helpers.createTransaction();

  expect(transaction).toMatchObject({
    amount: expect.any(String),
    date: expect.any(Date),
    business: expect.any(String),
    name: expect.any(String),
    type: expect.any(String),
    account: expect.any(String),
  });
});

it('amount should be in specific range', () => {
  const transaction = faker.helpers.createTransaction();

  expect(Number(transaction.amount)).toBeGreaterThanOrEqual(0);
  expect(Number(transaction.amount)).toBeLessThanOrEqual(1000);
});

it('account number to have specific length', () => {
  const transaction = faker.helpers.createTransaction();

  expect(transaction.account.length).toEqual(8);
});

it('type should be one of', () => {
  const types = ['deposit', 'withdrawal', 'payment', 'invoice'];
  const transaction = faker.helpers.createTransaction();

  expect(types).toContain(transaction.type);
});

it('transaction should be unique', () => {
  const transaction1 = faker.helpers.createTransaction();
  const transaction2 = faker.helpers.createTransaction();

  expect(transaction1).not.toMatchObject(transaction2);
  expect(transaction1).not.toBe(transaction2);
});
