class TaskPage {
  constructor(page) {
    this.page = page;
  }

  async add(content) {
    await this.page.type('[data-testid="task-name-input"]', content);
    await this.page.click('[data-testid="add-task-button"]');
  }

  async delete(id) {
    await this.page.click(`[data-testid="remove-task-${id}"]`);
  }
}

export default TaskPage;
