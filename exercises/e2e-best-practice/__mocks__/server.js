import { setupWorker, rest } from 'msw';
import _ from 'lodash';

const handlers = [
  rest.get('/tasks', (_req, res, ctx) => res(ctx.json([]))),
  rest.post('/tasks', (req, res, ctx) => {
    const id = _.uniqueId();
    const task = { ...req.body.task, id, state: 'active' };

    return res(ctx.json(task));
  }),
  rest.delete('/tasks/:id', (_req, res, ctx) => res(ctx.status(204))),
];

const worker = setupWorker(...handlers);

export default worker;
