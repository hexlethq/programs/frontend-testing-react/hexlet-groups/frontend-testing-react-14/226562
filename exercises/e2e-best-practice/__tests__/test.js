// @ts-check

import TaskPage from '../pages/TaskPage';

const port = 8080;
const appUrl = `http://localhost:${port}`;

describe('todo app', () => {
  beforeEach(async () => {
    await page.goto(appUrl, { waitUntil: 'domcontentloaded' });
  });

  test('can see the add task btn', async () => {
    await expect(page).toMatchElement('[data-testid="add-task-button"]');
  });

  test('can add task', async () => {
    await page.waitForSelector('[data-testid="add-task-button"]');
    const taskPage = new TaskPage(page);
    const content = 'new task';

    await taskPage.add(content);

    await expect(page).toMatch(content);
  });

  test('can delete task', async () => {
    await page.waitForSelector('[data-testid="add-task-button"]');
    const taskPage = new TaskPage(page);
    const content = 'new task';

    await taskPage.add(content);
    await expect(page).toMatch(content);
    await taskPage.delete(1);

    await expect(page).not.toMatch(content);
  });
});
