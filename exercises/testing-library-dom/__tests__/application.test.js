// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const faker = require('faker');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

const addTask = (name) => {
  const addTaskInput = screen.getByRole('textbox', { name: /new task name/i });
  userEvent.type(addTaskInput, name);
  userEvent.click(screen.getByRole('button', { name: /add task/i }));
};

const addList = (name) => {
  const addListInput = screen.getByRole('textbox', { name: /new list name/i });
  userEvent.type(addListInput, name);
  userEvent.click(screen.getByRole('button', { name: /add list/i }));
};

test('initial load', () => {
  expect(screen.getByText('General')).toBeVisible();
  expect(screen.getByTestId('tasks')).toBeEmptyDOMElement();
});

test('Add tasks to default list', () => {
  const firstTask = faker.lorem.word();
  const secondTask = faker.lorem.word();
  const newTaskInput = screen.getByRole('textbox', { name: /new task name/i });

  addTask(firstTask);
  addTask(secondTask);

  const taskContainer = screen.getByTestId('tasks');
  expect(taskContainer).toHaveTextContent(firstTask);
  expect(taskContainer).toHaveTextContent(secondTask);
  expect(newTaskInput).not.toHaveValue();
});

test('Add task to second list', () => {
  const newList = faker.lorem.word();
  const newListTask = faker.lorem.word();
  const defaultListTask = faker.lorem.word();

  addTask(defaultListTask);
  addList(newList);
  userEvent.click(screen.getByRole('link', { name: newList }));
  addTask(newListTask);

  const taskContainer = screen.getByTestId('tasks');
  expect(taskContainer).toHaveTextContent(newListTask);
  expect(taskContainer).not.toHaveTextContent(defaultListTask);
});
