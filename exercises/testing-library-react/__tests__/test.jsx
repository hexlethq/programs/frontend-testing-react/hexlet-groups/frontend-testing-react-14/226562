// @ts-check

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';
const cases = [
  ['a', ['afghanistan', 'albania', 'algeria']],
  ['al', ['albania', 'algeria']],
  ['alb', ['albania']],
];

beforeAll(() => {
  nock.disableNetConnect();
});

beforeEach(() => {
  cases.forEach(([term, countries]) => {
    nock(host)
      .get(`/countries?term=${term}`)
      .reply(200, countries);
  });
});

test.each(cases)('can see proper list of countries when search for: %s', async (term, countries) => {
  render(<Autocomplete />);
  const textBox = screen.getByRole('textbox');

  await userEvent.type(textBox, term, { delay: 100 });

  await waitFor(() => {
    countries.forEach((country) => {
      expect(screen.getByText(country)).toBeVisible();
    });

    const items = within(screen.getByTestId('countries')).getAllByRole('listitem');
    expect(items).toHaveLength(countries.length);
  });

  userEvent.clear(textBox);
  expect(screen.queryByTestId('countries')).not.toBeInTheDocument();
});
